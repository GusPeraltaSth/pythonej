name = "Goos"
x, book = 100, "I Robot"

print(name)
print(x, book)

#Conventions
book_name = "I Robot" #Snake Case
bookName = "I Robot" #Camel Case
BookName = "I Robot" #Pascal Case

#Constant
PI = 3.1416 #Por convención toto en mayuscula