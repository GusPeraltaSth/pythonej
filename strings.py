myStr = "Hello world"

# print(dir(myStr))

# print(myStr.upper())
# print(myStr.lower())
# print(myStr.swapcase())
# print(myStr.capitalize())
# print(myStr.replace("Hello", "bye"))
# print(myStr.count(' '))

# print(myStr.startswith('he'))
# print(myStr.endwith('world'))

# print(myStr.split('o'))
# print(myStr.find('z'))

# print(len(myStr))
# print(myStr.index('e'))

# print(myStr.isnumeric())
# print(myStr.isalpha())

print(myStr[0])
print("My name is "+myStr)
print(f"My name is {myStr}")
print("My name is {0}".format(myStr))
