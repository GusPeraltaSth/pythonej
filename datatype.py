#String
print("Hello world")
print('Hello world')
print("''Hello world''")
print("""Hello world""")
print('\'Hello world\'')

print("Bye"+" "+"World")

#Numbers
#Integer
print(19)

#Float
print(10.9)

#Boolean
True
False

#List
[10, 20, 30, 40]
['a', 'b', 'c']
[10, 'a', True] 
[]

#Tuples
(10, 20, 30, 40)
()

#Dictionaries
{
    "name":"Ryan",
    "lastname":"Ray",
    "nickname":"Goos"
}

None