product = {
    "name":"book",
    "quantity": 3,
    "price": 4.99
} 

person = {
    "first_name": "ryan",
    "last_name": "ray"
}

# print(type(product))
# print(type(person))

# print(person.keys())
# print(person.values())

person.clear()
print(person)

# del person
# print(person)