#function for atributes

class Person:
    age = 27
    name = 'Victor'
    country = 'Chile'

doctor = Person()

# print('Doctor\'s age is: ',doctor.age)
# print('Doctor\'s age is: ', getattr(doctor,'age'))

# print('el doctor tiene una edad?', hasattr(doctor,'age'))

# print(doctor.name)
# setattr(doctor,'name','gustavo')
# print(doctor.name)

# print(doctor.country)

# delattr(Person, 'country')
# print(hasattr(doctor.country))

