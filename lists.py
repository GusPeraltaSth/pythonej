demo_list = [1,2,'hello', 1.34,True,[1, 2, 3]]
colors = ['red', 'green', 'blue', 'red']

numbers_list = list((1, 2, 3, 4))

# r = list(range(1,10))
# print(r)

#print(dir(colors))

#print(len(colors))

#print('greens' in colors)

# colors.append('violet')
# colors.append(['violet', 'yellow'])
# colors.append(('violet', 'yellow'))

# colors.extend(['violet','yellow'])
# colors.extend(['pink','black'])

# colors.insert(-1,'violet')
# colors.insert(len(colors),'violet')

# colors.pop(1)
# colors.remove('blue')
# colors.clear()

colors.sort()
colors.sort(reverse=True)
colors.index('red')

print(colors.index('red'))
print(colors.count('red'))
print(colors)